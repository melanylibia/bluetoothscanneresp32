# BluetoothScannerESP32

This project is demo base to know how footprint tracking works.

**I WROTE A POST IN MEDIUM**

[Building an occupancy sensor with a ESP32: Scanner Bluetooth](https://medium.com/@elibialany/building-an-occupancy-sensor-with-a-esp32-scanner-bluetooth-49f7a4b724de)

![Image description](https://gitlab.com/melanylibia/bluetoothscanneresp32/-/raw/main/img/im.png)

## Usage

```
cd influxdb-proyect-00 
python3 -m venv .venv
. .venv/bin/activate
pip install -r requirements
cp .env .env.EXAMPLE
```

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
MIT licensed.

