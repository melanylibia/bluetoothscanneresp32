
import logging
import influxdb_client, os, time
from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS
from telegram import Update
from telegram.ext import ApplicationBuilder, ContextTypes, CommandHandler

###############################Telegram
TOKEN_TELEGRAM=os.getenv('TOKEN_TELEGRAM')
CHAT_ID_TELEGRAM=os.getenv('CHAT_ID_TELEGRAM')
############################### influx 
token = os.getenv('TOKEN_INFLUXDB')
org = os.getenv('ORG_INFLUXDB')
url = os.getenv('URL_INFLUXDB')
client = influxdb_client.InfluxDBClient(url=url, token=token, org=org)
################################
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)
def start_current_count_people(dias: str = '10'):
    try:
        query_api = client.query_api()
        query=f'from(bucket: "esp32")\
        |> range(start: -{dias}d)\
        |> filter(fn: (r) => r["_measurement"] == "wifi_status")\
        |> filter(fn: (r) => r["SSID"] == "r00thouse")\
        |> filter(fn: (r) => r["_field"] == "detected_devices")\
        |> filter(fn: (r) => r["device"] == "ESP32")\
        |> aggregateWindow(every: 7m, fn: mean, createEmpty: false)\
        |> yield(name: "mean")'
        return query_api.query(org=org, query=query)
    except Exception as e:
        print(e)
def current_count_people(dias: str = '10'):
    try: 
        result = start_current_count_people(dias)
        results = []
        for table in result:
            for record in table.records:
                results.append((record.get_field(), record.get_value()))
        cantidad = int(results[-1][1])
        return f'La cantidad de dispositivos son {cantidad}'
    except Exception as e:
        print(e)  
        return f"se produjo un error, intenta de nuevo"  

async def biblio_estado(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(chat_id=CHAT_ID_TELEGRAM, text="Hola!! " + current_count_people())

def is_number(s):
    return s.isdigit()

async def specific_day(update: Update, context: ContextTypes.DEFAULT_TYPE):
    args = context.args
    try:
        if len(args) > 0:
            if not is_number(args[0]):
                await context.bot.send_message(chat_id=CHAT_ID_TELEGRAM, text=f'La cantidad de dias debe ser un numero')
            else:
                await context.bot.send_message(chat_id=CHAT_ID_TELEGRAM, text=f"La cantidad de dispositivos en {args[0]} dias son {current_count_people(str(args[0]))}")
        else:
            await context.bot.send_message(chat_id=CHAT_ID_TELEGRAM, text='No especificaste el dia!')
    except Exception as e:
        print(e)
        await context.bot.send_message(chat_id=CHAT_ID_TELEGRAM, text='SE produjo un error, intente mas tarde')


if __name__ == '__main__':
    application = ApplicationBuilder().token(TOKEN_TELEGRAM).build()
    start_handler = CommandHandler('biblio_estado', biblio_estado)
    specific_day_handler = CommandHandler('biblio_dia_esp', specific_day)
    application.add_handler(start_handler)
    application.add_handler(specific_day_handler)
    application.run_polling()